tool
extends Node2D

export var origin = Vector2(0,0)
export var color = Color(1.0, 0.0, 0.0)

var shapes = []
var points = PoolVector2Array();

var currentShape = DrawingShape.new(color)
var nextShapeStart = 0

var mouseDown = false
var timer = Timer.new()
var last_point;

class DrawingShape:
	var color = Color(0.0,0.0,0.0)
	var pointsIndexes = []
	func _init(color):
		self.color = color

func _enter_tree():
	connect("pressed", self, "clicked")
	connect("mousemove", self, "clicked")

func clicked():
	print("hola")

func _ready():
	set_process_input(true)
	timer.connect("timeout", self, "_on_timer_timeout") 
	timer.one_shot = true
	timer.process_mode = Timer.TIMER_PROCESS_IDLE
	timer.set_wait_time(.01)
	add_child(timer)

func _input(event):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT:
		mouseDown = event.pressed
		if mouseDown:
			currentShape = DrawingShape.new(color)
			shapes.append(currentShape)
			update()
		elif currentShape.pointsIndexes.size() > 0:
			update()
	elif event is InputEventMouseMotion:
		if mouseDown:
			last_point = get_local_mouse_position()
			if timer.is_stopped():
				timer.start()

func _on_timer_timeout():
	if last_point:
		currentShape.pointsIndexes.append(points.size())
		points.append(last_point)
		last_point = false; 
		update()

func _draw():
	for shape in shapes:
		draw_shape(shape.pointsIndexes, shape.color)

func draw_shape(indexes, color, width=5.0, close=false):
	var count = indexes.size()
	print(indexes, points)
	for index_point_index in range(count - 1 ):
		var index_point = indexes[index_point_index]
		var next_index_point = indexes[index_point_index+1]
		var start = origin + points[index_point];
		var end = origin + points[next_index_point];
		draw_line(start, end, color, width, true)

	if close:
		draw_line(origin + points[indexes[count-1]], origin + points[indexes[0]], color, width, true)